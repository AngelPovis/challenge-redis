package com.globant.redis.challenge.controller

import com.globant.redis.challenge.domain.User
import com.globant.redis.challenge.dto.SaveUser
import org.springframework.data.redis.core.ReactiveRedisOperations
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@RestController
class UserController(val userOps: ReactiveRedisOperations<String, User>) {

    @GetMapping("/users")
    fun all(): Flux<User> {
        return userOps.keys("*")
            .flatMap(userOps.opsForValue()::get)
    }

    @PostMapping("/users")
    fun all(@RequestBody user: SaveUser): Mono<ResponseEntity<SaveUser>> {
        return userOps.hasKey(user.email).filter { !it }.flatMap {
            userOps.opsForValue().set(user.email, User(user.name, user.email))
        }.map { ResponseEntity.ok(user) }
            .defaultIfEmpty(ResponseEntity.badRequest().build())
    }

    @GetMapping("/users/{key}")
    fun findUserById(@PathVariable key: String): Mono<ResponseEntity<User>> {
        return userOps.opsForValue().get(key).map { ResponseEntity.ok(it) }
            .defaultIfEmpty(ResponseEntity.noContent().build())
    }

    @DeleteMapping("/users/{key}")
    fun deleteUser(@PathVariable key: String): Mono<ResponseEntity<Void>> {
        return userOps.opsForValue().delete(key).map { ResponseEntity.noContent().build() }
    }
}