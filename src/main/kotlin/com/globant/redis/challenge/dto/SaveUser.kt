package com.globant.redis.challenge.dto

import javax.validation.constraints.Email

data class SaveUser(val name: String, @Email val email: String) {

}