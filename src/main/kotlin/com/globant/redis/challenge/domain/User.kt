package com.globant.redis.challenge.domain

data class User(
    val name: String,
    val email: String
) {
    constructor() : this("", "") {
    }
}