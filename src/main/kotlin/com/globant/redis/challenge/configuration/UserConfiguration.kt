package com.globant.redis.challenge.configuration

import com.globant.redis.challenge.domain.User
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.redis.core.ReactiveRedisOperations
import org.springframework.data.redis.core.ReactiveRedisTemplate
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer
import org.springframework.data.redis.serializer.RedisSerializationContext
import org.springframework.data.redis.serializer.RedisSerializationContext.RedisSerializationContextBuilder
import org.springframework.data.redis.serializer.StringRedisSerializer


@Configuration
class UserConfiguration() {

    @Bean
    fun redisOperations(factory: ReactiveRedisConnectionFactory): ReactiveRedisOperations<String, User>? {
        val serializer: Jackson2JsonRedisSerializer<User> = Jackson2JsonRedisSerializer<User>(User::class.java)

        val builder: RedisSerializationContextBuilder<String, User> =
            RedisSerializationContext.newSerializationContext<String, User>(StringRedisSerializer())

        val context: RedisSerializationContext<String, User> = builder.value(serializer).build()

        return ReactiveRedisTemplate(factory, context)
    }
}